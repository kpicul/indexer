import time
from pathlib import Path

import nltk

from Posting import Posting
from Worker import Worker
from database import Database
print("Results for a query: ")
words = input()
start_time = time.time()
wordList = words.split(" ")
data = Database()
entries = []
for word in wordList:
    entries += data.getPostings(word)
postings = []
for el in entries:
    insrt = Posting()
    insrt.insertEntry(el)
    postings.append(insrt)
positions = {}
freqs = {}
for el in postings:
    #print(el.getDocName())
    if positions.get(el.getDocName(), False) == False:
        positions[el.getDocName()] = el.getIndexes()
        freqs[el.getDocName()] = el.getFrequency()
    else:
        positions[el.getDocName()] += el.getIndexes()
        freqs[el.getDocName()] += el.getFrequency()
for el in positions:
    indxs = sorted(positions[el])
    positions[el] = indxs
snippets = {}
for pos in positions:
    docData = data.getDocumentPostings(pos)
    docPostingsList = []
    for doc in docData:
        post = Posting()
        post.insertEntry(doc)
        docPostingsList.append(post)
    for ind in positions[pos]:
        snippetMaker = "..."
        for i in range(ind-3, ind+4):
            if i > 0:
                for post in docPostingsList:
                    if post.hasIndex(i):
                        snippetMaker+=post.getWord()+" "
        snippetMaker+="..."
        if snippets.get(pos, False) == False:
            snippets[pos] = []
            snippets[pos].append(snippetMaker)
        else:
            if len(snippets[pos]) < 5:
                snippets[pos].append(snippetMaker)
# for el in snippets:
#     print(el, ":", snippets[el])
sorted_x = sorted(freqs.items(), key=lambda kv: kv[1], reverse=True)
print("Results found in %  seconds" % (round(time.time() - start_time, 3)))
print("Frequencies     Document          Snippet")
print("-----------------------------------------------------------------------------------------------------------------------")
for el in sorted_x:
    snipp = ""
    for snip in snippets[el[0]]:
        snipp+=snip+" "
    print(freqs[el[0]],"     ", el[0], "          ", snipp)