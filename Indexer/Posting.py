class Posting:
    def __init__(self, word="", docName=""):
        self.word = word
        self.docName = docName
        self.frequency = 1
        self.indexes = []

    def setFreq(self, freq):
        self.frequency = freq

    def increaseFrequency(self):
        self.frequency+=1

    def addIndex(self, index):
        self.indexes.append(index)

    def indexesToString(self):
        strIndex = ""
        i=0
        for index in self.indexes:
            strIndex += str(index)
            if i != len(self.indexes)-1:
                strIndex+=", "
            i+=1
        return strIndex

    def getWord(self):
        return self.word

    def getDocName(self):
        return self.docName

    def getFrequency(self):
        return self.frequency

    def toString(self):
        return "'"+self.word+"','"+self.docName+"','"+str(self.frequency)+"','"+self.indexesToString()+"'"

    def insertEntry(self, list):
        self.word = list[0]
        self.docName = list[1]
        self.frequency = int(list[2])
        indList = list[3].split(",")
        for el in indList:
            self.indexes.append(int(el))
    def setIndexes(self, indexes):
        self.indexes = indexes

    def getIndexes(self):
        return self.indexes

    def hasIndex(self, index):
        for ind in self.indexes:
            if ind == index:
                return True
        return False