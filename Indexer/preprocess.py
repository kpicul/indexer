import string

from nltk import word_tokenize

from stopwords import stop_words_slovene

def removeStopWords(text):
    tokens = word_tokenize(text.translate(str.maketrans('', '', string.punctuation)).lower())
    fsentence = [w for w in tokens if not w in stop_words_slovene]
    return fsentence