# DESCRIPTION
This is a indexer and data retriaval program that processes the data from html files

# SETUP
* First you should have python 3.6 installed in your system. In venv folder is
* the python virtual environment that already includes all the libraries. On macOs
* and Windows chromedriver should be downloaded from http://chromedriver.chromium.org/downloads
* and then path should be correcte in file worker.py in init. Alternatively you could use 
* your own python virtual env that has nltk and selenium libraries installed. Folder PA3-data 
* should be put into indexer folder.

# USAGE
* Run Python indexer.py to use indexer (warning database in inverted-index.db should be emptied first). Use Python retrieval_index.py to use 
* data retrieval with index and Python retrieval.py to use retrieval without index.