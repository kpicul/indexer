import time
from pathlib import Path

import nltk

from Posting import Posting
from Worker import Worker
from database import Database
print("Results for a query: ")
words = input()
start_time = time.time()
wordList = words.split(" ")
freqs = {}
snippets = {}
domains = [("e-prostor.gov.si", 218), ("e-uprava.gov.si", 60), ("evem.gov.si", 662), ("podatki.gov.si", 564)]
for domain in domains:
    parentFolder = domain[0]
    for no in range(1, domain[1] + 1):
        freq = 0
        filname = parentFolder + "." + str(no) + ".html"
        print(filname)
        worker = Worker()
        pathUrl = "PA3-data/" + parentFolder + "/" + filname
        html_file = Path.cwd() / pathUrl
        text = worker.getText(html_file.as_uri())
        worker.quit()
        tokens = nltk.word_tokenize(text)
        indices = []
        ind = 0
        for token in tokens:
            for word in wordList:
                if word.lower() == token.lower().replace(".","").replace(",","").replace(";","").replace(":",""):
                    freq+=1
                    indices.append(ind)
            ind+=1

        for ind in indices:
            snipp = "..."
            for i in range(ind-3, ind+4):
                if i > 0:
                    snipp+= tokens[i]+" "
                snipp +="..."
            if snippets.get(filname, False) == False:
                snippets[filname] = []
                snippets[filname].append(snipp)
            else:
                snippets[filname].append(snipp)
        freqs[filname] = freq
sorted_x = sorted(freqs.items(), key=lambda kv: kv[1], reverse=True)
print("Results found in %  seconds" % (round(time.time() - start_time, 3)))
for el in sorted_x:
    print("Frequencies     Document          Snippet")
    print("-----------------------------------------------------------------------------------------------------------------------")
    snipp = ""
    for snip in snippets[el[0]]:
        snipp+=snip
    print(freqs[el[0]],"     ", el[0], "          ", snipp)