import sqlite3
class Database:
    def __init__(self):
        self.conn = sqlite3.connect('inverted-index.db')
        self.cursor = self.conn.cursor()

    def insertWords(self, words):
        self.cursor.executemany("INSERT INTO IndexWord VALUES (?)", list(words))
        self.conn.commit()

    def insertPostings(self, postingsDict):
        postingsList = []
        for posting in postingsDict:
            word = postingsDict[posting].getWord()
            docName = postingsDict[posting].getDocName()
            freq = postingsDict[posting].getFrequency()
            indexes = postingsDict[posting].indexesToString()
            postingsList.append([word, docName, freq, indexes])
        self.cursor.executemany("INSERT INTO Posting VALUES (?,?,?,?)", postingsList)
        self.conn.commit()

    def getPostings(self, word):
        t = (word,)
        self.cursor.execute("SELECT * FROM POSTING  WHERE word = ?", t)
        return self.cursor.fetchall()

    def getDocumentPostings(self, docName):
        t = (docName,)
        self.cursor.execute("SELECT * FROM POSTING  WHERE documentName = ?", t)
        return self.cursor.fetchall()