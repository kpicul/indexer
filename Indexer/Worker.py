from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class Worker:
    def __init__(self):
        chromeOp = Options()
        chromeOp.add_argument("--headless")
        self.worker = webdriver.Chrome("chromedriver_linux64/chromedriver", chrome_options=chromeOp)

    def getText(self, url):
        el = None
        try:
            self.worker.get(url)
            el = self.worker.find_element_by_xpath("//body")
        except:
            print("Error")
        return el.text

    def quit(self):
        self.worker.quit()