from pathlib import Path

import nltk

import preprocess
from Posting import Posting
from Worker import Worker
from database import Database

def getIndexes(word, tokens):
    indexes = []
    windex = 0
    for el in tokens:
        if el.lower() == word.lower():
            indexes.append(windex)
        windex+=1
    return indexes

domains = [("e-prostor.gov.si", 218), ("e-uprava.gov.si", 60), ("evem.gov.si", 662), ("podatki.gov.si", 564)]
words = set()
db = Database()
for domain in domains:
    parentFolder = domain[0]
    for no in range (1, domain[1]+1):
        filname = parentFolder+"."+str(no)+".html"
        print(filname)
        worker = Worker()
        pathUrl = "PA3-data/"+parentFolder+"/"+filname
        html_file = Path.cwd() / pathUrl
        text = worker.getText(html_file.as_uri())
        tokens = nltk.word_tokenize(text)
        #print(text)
        worker.quit()
        textList = preprocess.removeStopWords(text)
        # print("\n")
        # print(text)
        postingsLocal = {}
        localWords = []
        windx= 0
        for word in textList:
            if word not in words:
                words.add(word)
                localWords.append([word])
            if postingsLocal.get(word, False) == False:
                postingsLocal[word] = Posting(word, filname)
                postingsLocal[word].addIndex(windx)
            else:
                postingsLocal[word].increaseFrequency()
                postingsLocal[word].addIndex(windx)
            windx+=1
        for posting in postingsLocal:
            # indxs = getIndexes(postingsLocal[posting].getWord(), tokens)
            # if len(indxs) == 0:
            #     indxs.append(0)
            # postingsLocal[posting].setIndexes(indxs)
            print(postingsLocal[posting].toString())
        db.insertWords(localWords)
        db.insertPostings(postingsLocal)